#ifndef FILE_READER_H
#define FILE_READER_H

#include "bank_operations.h"

void read(const char* file_name, bank_operations* array[], int& size);

#endif