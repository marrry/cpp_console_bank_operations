#include <iostream>
#include <iomanip>

using namespace std;

#include "bank_operations.h"
#include "file_reader.h"
#include "constants.h"

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "Laboratory work #8. GIT\n";
    cout << "Variant #7. Bank Operations\n";
    cout << "Author: Mary Tarasova\n";
    cout << "Group: 12\n";
    bank_operations* operations[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", operations, size);
        cout << "***** ���������� �������� *****\n\n";
        for (int i = 0; i < size; i++)
        {
            /********** ����� ���� � ������� **********/
            cout << "���� � �����....: ";
            //����� ����
            cout << setw(4) << setfill('0') << operations[i]->date.year << '-';
            //����� ������
            cout << setw(2) << setfill('0') << operations[i]->date.month << '-';
            //����� �����
            cout << setw(2) << setfill('0') << operations[i]->date.day;
            cout << "  ";
            //����� �������
            cout << setw(2) << setfill('0') << operations[i]->time.hours << ':';
            cout << setw(2) << setfill('0') << operations[i]->time.minutes << ':';
            cout << setw(2) << setfill('0') << operations[i]->time.seconds;
            cout << '\n';
            /********** ����� ���������� **********/
            //����� ���� ��������
            cout << "��� ��������....: ";
            cout << operations[i]->type << '\n';
            //����� ������ �����
            cout << "����� �����.....: ";
            cout << operations[i]->account << '\n';
            //����� ����� �������
            cout << "�����...........: ";
            cout << operations[i]->sum << '\n';
            //����� ���������� ��������
            cout << "����������......: ";
            cout << operations[i]->aim << '\n';
            cout << '\n';
        }
        for (int i = 0; i < size; i++)
        {
            delete operations[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}
